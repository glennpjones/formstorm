defmodule Formstorm.Repo.Migrations.CreateResponse do
  use Ecto.Migration

  def change do
    create table(:responses) do
      add :form_id, references(:forms)
      add :fields, {:map, :string}
      add :meta, {:map, :string}

      timestamps()
    end

  end
end
