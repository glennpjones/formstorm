defmodule Formstorm.Repo.Migrations.AddSpamFieldToResponses do
  use Ecto.Migration

  def change do
    alter table(:responses) do
      add :spamminess, :float, default: 1
    end
  end
end
