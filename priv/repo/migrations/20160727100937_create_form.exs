defmodule Formstorm.Repo.Migrations.CreateForm do
  use Ecto.Migration

  def change do
    create table(:forms) do
      add :user_id, references(:users)
      add :key, :string
      add :domain, :string
      add :name, :string
      add :description, :text

      timestamps()
    end

  end
end
