# Formstorm

## Rationale

### Why?
Form data isn’t complex and neither is processing it. Forms, certainly in 2016, are not difficult and you definitely don’t need a SaaS to deal with them. You can keep the data safely on your own server and you can do it cheaply as well, because of elixir.

Formstorm seeks to provide a simple collection and access point for all your form data. You can add as many forms as you’d like.

### Benefits
* data on your own servers
* only you have access to the data
* cheap (only pay for the server - can be for free)
* cheap (elixir allows high throughput of responses, allowing you to use a cheap server)
* easy to maintain and extend

## Development roadmap:
	* https://trello.com/b/arueAPB5/formstorm-dev

## To start the app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
