# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :formstorm,
  ecto_repos: [Formstorm.Repo]

# Configures the endpoint
config :formstorm, Formstorm.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "t8Y8xomcqKs7myNCi/SjyyPn0eURdpC0qGoXbBGy/9ChbVqxK+6hkIik87fKJgI0",
  render_errors: [view: Formstorm.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Formstorm.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

config :addict,
  secret_key: "243262243132246b786f75776830786f33557151303162726862754265",
  extra_validation: fn ({valid, errors}, user_params) -> {valid, errors} end, # define extra validation here
  user_schema: Formstorm.User,
  repo: Formstorm.Repo
  # from_email: "no-reply@example.com", # CHANGE THIS
  # mailgun_domain: "",
  # mailgun_key: "",
  # mail_service: :mailgun
