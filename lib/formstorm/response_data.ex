defmodule Formstorm.ResponseData do

  def extract_metadata(conn) do
    Map.new
    |> Map.merge(extract_req_headers(conn))
    |> Map.merge(extract_ip(conn))
  end

  def extract_req_headers(conn) do
    conn.req_headers
    |> Enum.filter(fn({k,_}) -> k=="host" ||
                                k=="origin" ||
                                k=="referer" ||
                                k=="user-agent" ||
                                k=="accept-language" end)
    |> Enum.into(Map.new)
  end

  def extract_ip(conn) do
    conn.remote_ip
    |> Tuple.to_list
    |> Enum.map(fn(digit) -> to_string(digit) end)
    |> Enum.join(".")
    |> (fn(ip) -> %{"ip" => ip} end).()
  end

  def format_response(params) do
    params
    |> Map.drop(["path"])
  end
end
