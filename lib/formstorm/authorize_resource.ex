defmodule Formstorm.AuthorizeResource do
  @moduledoc "Validates that a resource belongs to a user. Must provide the attribute 'resource' as option. Implemented for Form."
  require Logger
  
  use Phoenix.Controller

  import Plug.Conn
  import Addict.Helper

  alias Formstorm.Form
  alias Formstorm.Repo

  def init(opts) do
    opts
  end

  def call(conn, opts \\ %{}) do
    # get resource type, get its id, get current user
    resource = Map.fetch!(opts, :resource)
    %Plug.Conn{params: %{"id" => resource_id}} = conn
    user_id = current_user(conn).id
    Logger.debug("Authorizing usage of resource: #{resource} with id: #{resource_id} by user with id: #{user_id}")

    # depending on the resource type, do validation
    case resource do
      Elixir.Formstorm.Form ->
        form = Form |> Form.by_id(resource_id) |> Repo.all |> List.first
        case user_id == form.user_id do
          true ->
            Logger.debug("Resource authorization succesfull.")
            conn
          false ->
            Logger.debug("Resource authorization unsuccesfull.")
            conn
            |> put_flash(:error, "Oh no, that form does not seem to belong to you.")
            |> redirect(to: "/forms")
            |> halt
      _ ->
        # no resource type provided, make error clear to developer
        end
    end
  end
end
