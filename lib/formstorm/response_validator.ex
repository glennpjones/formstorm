defmodule Formstorm.ResponseValidator do
  require Logger

  import Ecto.Changeset
  import Ecto.Query

  alias Formstorm.Form
  alias Formstorm.Repo

  def set_spamminess(changeset) do 
    form_id = (get_field(changeset, :form_id))
    formdomain = Repo.get!(Form, form_id).domain
    # determine the spamvalue
    metadata = get_field(changeset, :meta)
    spamvalue = determine_spamvalue(metadata, formdomain)
    # return updated changeset
    put_change(changeset, :spamminess, spamvalue)
  end

  def determine_spamvalue(metadata, formdomain) do
    Logger.debug("Metadata: #{inspect(metadata)}")
    case metadata do
      %{"referer" => referer, "host" => host, "origin" => origin} ->
        spamvalue = compare_urls(formdomain, host)
      %{"referer" => referer, "host" => host} ->
        spamvalue = compare_urls(formdomain, host)
      %{"host" => host} ->
        spamvalue = compare_urls(formdomain, host)
      %{"referer" => referer} ->
        # below is placeholder value
        spamvalue = 1.0
      %{"origin" => origin} ->
        # below is placeholder value
        spamvalue = 1.0
      %{_: _} ->
        Logger.debug("No referer, host or origin found. Spamvalue set to 1.0")
        spamvalue = 1.0
    end
  end

  def compare_urls(url1, url2) do
    Logger.debug("Comparing urls: url1: '#{url1}', url2: '#{url2}")
    case url1 == url2 do
      true  ->
        0.0
      _ ->
        1.0
    end
  end

end
