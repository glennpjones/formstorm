defmodule Formstorm.FormController do
  use Formstorm.Web, :controller
  import Ecto.Changeset, only: [cast: 3]
  import Addict.Helper

  plug Addict.Plugs.Authenticated
  plug Formstorm.AuthorizeResource, %{resource: Formstorm.Form} when action in [:show, :edit, :update, :delete]

  alias Formstorm.Form
  alias Formstorm.Response

  def index(conn, _params) do
    user_id = current_user(conn).id
    forms = Form |> Form.by_user(user_id) |> Repo.all

    render(conn, "index.html", forms: forms)
  end

  def new(conn, _params) do
    changeset = Form.changeset(%Form{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"form" => form_params}) do
    changeset = Form.changeset(%Form{}, form_params)
    |> cast(%{user_id: current_user(conn).id, key: Form.generate_key}, [:user_id, :key])

    case Repo.insert(changeset) do
      {:ok, _form} ->
        conn
        |> put_flash(:info, "Form created successfully.")
        |> redirect(to: form_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    {form_id,_} = Integer.parse(id)

    form = Form |> Form.by_id(form_id) |> Repo.all |> List.first
    responses = Response |> Response.by_form_id(form_id) |> Response.sort_by_creation |> Repo.all

    render(conn, "show.html", form: form, responses: responses)
  end

  def edit(conn, %{"id" => id}) do
    form = Repo.get!(Form, id)
    changeset = Form.changeset(form)
    render(conn, "edit.html", form: form, changeset: changeset)
  end

  def update(conn, %{"id" => id, "form" => form_params}) do
    form = Repo.get!(Form, id)
    changeset = Form.changeset(form, form_params)

    case Repo.update(changeset) do
      {:ok, form} ->
        conn
        |> put_flash(:info, "Form updated successfully.")
        |> redirect(to: form_path(conn, :show, form))
      {:error, changeset} ->
        render(conn, "edit.html", form: form, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    form = Repo.get!(Form, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(form)

    conn
    |> put_flash(:info, "Form deleted successfully.")
    |> redirect(to: form_path(conn, :index))
  end
end
