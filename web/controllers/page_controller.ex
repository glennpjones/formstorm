defmodule Formstorm.PageController do
  use Formstorm.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
