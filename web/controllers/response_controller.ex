defmodule Formstorm.ResponseController do
  use Formstorm.Web, :controller
  require Logger
  import Formstorm.ResponseData, only: [extract_metadata: 1, format_response: 1]

  alias Formstorm.Response
  alias Formstorm.Form

  def create(conn, params = %{}) do

    form = find_form(params)
    
    changeset = Response.changeset(%Response{}, %{form_id: form.id, fields: format_response(params), meta: extract_metadata(conn), spamminess: 1})
    Logger.debug("Generated changeset for response:")
    Logger.debug(inspect(changeset))
    
    case Repo.insert(changeset) do
      {:ok, _response} ->
        conn
        |> redirect(to: page_path(conn, :index))
      {:error, _changeset} ->
        conn
        |> redirect(to: page_path(conn, :index))
    end

  end

  def find_form(params) do
    key = params["path"] |> to_string
    Repo.get_by(Form, key: key)
  end
end
