defmodule Formstorm.Response do
  use Formstorm.Web, :model
  use Ecto.Schema

  import Formstorm.ResponseValidator, only: [set_spamminess: 1]

  schema "responses" do
    belongs_to :form, Formstorm.Form
    field :fields, {:map, :string}
    field :meta, {:map, :string}
    field :spamminess, :float

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:form_id, :fields, :meta])
    |> validate_required([:form_id, :fields, :meta])
    |> set_spamminess
  end

  @doc "Select responses by their form_id"
  def by_form_id(query, form_id) do
    from r in query,
    join: f in assoc(r, :form),
    where: f.id == ^form_id,
    select: r
  end

  @doc "Sort responses by inserted_at attribute" 
  def sort_by_creation(query) do
    from r in query,
    order_by: [desc: r.inserted_at]
  end

end
