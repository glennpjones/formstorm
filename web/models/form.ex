defmodule Formstorm.Form do
  use Formstorm.Web, :model
  use Ecto.Schema

  schema "forms" do
    belongs_to :user, Formstorm.User
    has_many :responses, Formstorm.Response
    field :key, :string
    field :domain, :string
    field :name, :string
    field :description, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:user_id, :key, :domain, :name, :description])
    |> validate_required([:domain, :name, :description])
  end

  def generate_key do
    chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" |> String.split("")

    Enum.reduce((1..8), [], fn (_i, acc) ->
      [Enum.random(chars) | acc]
    end) |> Enum.join("")
  end

  @doc "Select forms by user id"
  def by_user(query, user_id) do
    from f in query,
    where: f.user_id == ^user_id,
    select: %{id: f.id, key: f.key, domain: f.domain, name: f.name, description: f.description}
  end

  @doc "Select form by its id"
  def by_id(query, form_id) do
    from f in query,
    where: f.id == ^form_id,
    select: %{id: f.id, user_id: f.user_id, key: f.key, domain: f.domain, name: f.name, description: f.description}
  end

end
