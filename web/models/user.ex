defmodule Formstorm.User do
  use Formstorm.Web, :model
  use Ecto.Schema

  schema "users" do
    has_many :forms, Formstorm.Form

    field :email, :string
    field :encrypted_password, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:email, :encrypted_password])
    |> validate_required([:email, :encrypted_password])
  end
end
