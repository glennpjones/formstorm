defmodule Formstorm.Router do
  use Formstorm.Web, :router
  use Addict.RoutesHelper

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Formstorm do
    pipe_through :browser # Use the default browser stack

    get "/submitform", PageController, :index
    resources "/forms", FormController
    get "/", FormController, :index
  end

  scope "/" do
    addict :routes
  end

  post "/*path", Formstorm.ResponseController, :create

  # Other scopes may use custom stacks.
  # scope "/api", Formstorm do
  #   pipe_through :api
  # end
end
