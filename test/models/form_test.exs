defmodule Formstorm.FormTest do
  use Formstorm.ModelCase

  alias Formstorm.Form

  @valid_attrs %{description: "some content", domain: "some content", key: "some content", name: "some content", user_id: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Form.changeset(%Form{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Form.changeset(%Form{}, @invalid_attrs)
    refute changeset.valid?
  end
end
