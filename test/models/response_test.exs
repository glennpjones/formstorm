defmodule Formstorm.ResponseTest do
  use Formstorm.ModelCase

  alias Formstorm.Response

  @valid_attrs %{fields: "some content", form_id: "some content", meta: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Response.changeset(%Response{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Response.changeset(%Response{}, @invalid_attrs)
    refute changeset.valid?
  end
end
